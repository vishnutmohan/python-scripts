import os
import re


def main():
    for file_name in os.listdir(os.path.dirname(os.path.abspath(__file__))):
        if file_name.endswith("mp4"):
            print file_name
            digit_with_comma_dot = re.search("-\d+.mp4", file_name)

            str_digit_with_comma_dot = str(digit_with_comma_dot.group())

            digit_only = re.search("\d+", str_digit_with_comma_dot)

            str_digit_only = str(digit_only.group())

            file_name_without_digit = re.sub(str_digit_with_comma_dot, '', file_name)

            corrected_file_name = str_digit_only + "-" + file_name_without_digit + ".mp4"
            
            os.rename(file_name, corrected_file_name)


if __name__ == '__main__':
    main()

